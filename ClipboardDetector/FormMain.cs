﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ClipboardDetector
{
    public partial class FormMain : Form
    {
        #region FileStatus
        private string FilePath = "";
        private bool Changed = false;
        #endregion
        Thread de_t;

        public FormMain()
        {
            InitializeComponent();
        }
        private void ReloadThread()
        {
        	de_t = new Thread(new ThreadStart(Detector));
            de_t.IsBackground = true;
            de_t.Priority = ThreadPriority.Normal;
        }
        private void NewFile()
        {
            ClearAll();
            FilePath = "";
            Changed = false;
        }
        private void OpenFile(string path)
        {
        	ClearAll();
            try
            {
                using(StreamReader sr = new StreamReader(path))
                {
	                while(!sr.EndOfStream)
	                {
	                	string s = sr.ReadLine();
	                	listBox1.Items.Add(s);
	                }
	                sr.Close();
                }                             
                FilePath = path;
                Changed = false;
            }
            catch (IOException ex)
            {
            	MessageBox.Show(ex.ToString(), ex.GetType().Name, MessageBoxButtons.OK, MessageBoxIcon.Error);
            } 
        }
        private void SaveFile(string path)
        {
        	try 
        	{
	            using(StreamWriter sr = new StreamWriter(path))
	            {
	            	foreach (string item in listBox1.Items) {
	            		sr.WriteLine(item);
	            	}
	            	sr.Flush();
		            sr.Close();
	            }
	            FilePath = path;
            	Changed = false;
        	} 
        	catch (IOException ex) 
        	{
        		MessageBox.Show(ex.ToString(), ex.GetType().Name, MessageBoxButtons.OK, MessageBoxIcon.Error);
        	}			
        }
        private void Start()
        {
            try
            {
            	ReloadThread();
                de_t.Start();
            }
            catch (ThreadStateException ex)
            {
                MessageBox.Show("目前無法啟動執行緒\nThreadState: " + de_t.ThreadState.ToString(), ex.GetType().Name, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void Stop()
        {
            try
            {
                de_t.Abort();
            }
            catch (ThreadStateException ex)
            {
                MessageBox.Show("目前無法停止執行緒\nThreadState: " + de_t.ThreadState.ToString(), ex.GetType().Name, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void Cut()
        {
            Copy();
            Remove();
        }
        private void Copy()
        {
        	laststr = listBox1.SelectedItem.ToString();
            Clipboard.SetText(listBox1.SelectedItem.ToString());
        }
        private void Paste()
        {
            if (Clipboard.ContainsText())
            {
                listBox1.Items.Add(Clipboard.GetText());
                Changed = true;
            }
        }
        private void Sort(bool Ascending)
        {
            Changed = true;
        }
        private void Remove()
        {
            listBox1.Items.RemoveAt(listBox1.SelectedIndex);
            Changed = true;
        }
        private void ClearAll()
        {
            listBox1.Items.Clear();
            Changed = true;
        }
	
		string lastchk;
		string laststr;
        private void ChkClip()
        {
        	lastchk = Clipboard.GetText();
        }
        private void Detector()
        {
            try
            {
            	Invoke(new Action(ChkClip));
                laststr = lastchk;            
                while (true)
                {
                	Invoke(new Action(ChkClip));
                	//Debug.Print("laststr: {0} , get: {1}",laststr,lastchk);
                    if (laststr != lastchk)
                    {
                        Invoke(new Action(Paste));
                        laststr = lastchk;
                    }
                    Thread.Sleep(100);
                }
            }
            catch (ThreadAbortException)
            {
                return;
            }
            catch (Exception)
            {
                throw;
            }
        }
        private DialogResult SaveQuery()
        {
        	DialogResult dr = MessageBox.Show("此檔案已經變更過，是否儲存？","問題",MessageBoxButtons.YesNoCancel,MessageBoxIcon.Exclamation,MessageBoxDefaultButton.Button1);
        	switch (dr) {
        		case DialogResult.Cancel:
        			return dr;
        		case DialogResult.Yes:
        			儲存SToolStripButton.PerformClick();
        			if(Changed)return DialogResult.Cancel;
        			break;
        		case DialogResult.No:
        			break;
        	}
        	return dr;
        }
        private void RefreshPaste()
        {
        	if(Clipboard.ContainsText())
        	{
        		貼上PToolStripButton.Enabled = true;
        	}
        	else
        	{
        		貼上PToolStripButton.Enabled = false;
        	}
        }
        
        void 新增NToolStripButton_Click(object sender, EventArgs e)
        {
        	if(Changed)
        	{
        		if(SaveQuery()== DialogResult.Cancel)return;
        	}
        	NewFile();
        }
        
        void 開啟OToolStripButton_Click(object sender, EventArgs e)
        {
			if(Changed)
        	{
				if(SaveQuery()== DialogResult.Cancel)return;
        	}
			if(openFileDialog1.ShowDialog() == DialogResult.OK)
			{ 
				OpenFile(openFileDialog1.FileName);
			}
        }
              
        void 儲存SToolStripButton_Click(object sender, EventArgs e)
        {
        	if(FilePath=="")
        	{
        		toolStripButton1.PerformClick();
        		return;
        	}
        	SaveFile(FilePath);
        }
        
        void ToolStripButton1_Click(object sender, EventArgs e)
        {
        	if(saveFileDialog1.ShowDialog() == DialogResult.OK)
        	{
        		SaveFile(saveFileDialog1.FileName);
        	}
        }
        
        
        void StartStripButton_Click(object sender, EventArgs e)
        {
        	Start();
        	StartStripButton.Enabled = false;
        	StopStripButton.Enabled = true;
        	新增NToolStripButton.Enabled = false;
        	開啟OToolStripButton.Enabled = false;
        	儲存SToolStripButton.Enabled = false;
        	toolStripButton1.Enabled = false;
        }
        
        void StopStripButton_Click(object sender, EventArgs e)
        {
        	Stop();
        	StartStripButton.Enabled = true;
        	StopStripButton.Enabled = false;
        	新增NToolStripButton.Enabled = true;
        	開啟OToolStripButton.Enabled = true;
        	儲存SToolStripButton.Enabled = true;
        	toolStripButton1.Enabled = true;
        }
        
        void 剪下UToolStripButton_Click(object sender, EventArgs e)
        {
        	Cut();
        	RefreshPaste();
        }
        
        void 複製CToolStripButton_Click(object sender, EventArgs e)
        {
        	Copy();
        	RefreshPaste();
        }
        
        void 貼上PToolStripButton_Click(object sender, EventArgs e)
        {
        	Paste();
        }
        
        void ListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
        	if(listBox1.SelectedIndex!=-1)
        	{
        		剪下UToolStripButton.Enabled = true;
        		複製CToolStripButton.Enabled = true;
        		toolStripButton4.Enabled = true;
        	}
        	else
        	{
        		剪下UToolStripButton.Enabled = false;
        		複製CToolStripButton.Enabled = false;
        		toolStripButton4.Enabled = false;
        	}
        }
        
        void FormMain_Activated(object sender, EventArgs e)
        {
        	RefreshPaste();
        }
        
        void ToolStripButton4_Click(object sender, EventArgs e)
        {
        	Remove();
        }
        
        void ToolStripButton5_Click(object sender, EventArgs e)
        {
        	if(MessageBox.Show("清除剪貼簿？","問題",MessageBoxButtons.YesNo,MessageBoxIcon.Exclamation,MessageBoxDefaultButton.Button2)==DialogResult.Yes)
        		ClearAll();
        }
        
        void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
        	if(Changed)
        	{
				if(SaveQuery()== DialogResult.Cancel)e.Cancel = true;
        	}
        }
    }
}
