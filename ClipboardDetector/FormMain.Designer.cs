﻿namespace ClipboardDetector
{
    partial class FormMain
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
        	System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
        	this.toolStrip1 = new System.Windows.Forms.ToolStrip();
        	this.新增NToolStripButton = new System.Windows.Forms.ToolStripButton();
        	this.開啟OToolStripButton = new System.Windows.Forms.ToolStripButton();
        	this.儲存SToolStripButton = new System.Windows.Forms.ToolStripButton();
        	this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
        	this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
        	this.StartStripButton = new System.Windows.Forms.ToolStripButton();
        	this.StopStripButton = new System.Windows.Forms.ToolStripButton();
        	this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
        	this.剪下UToolStripButton = new System.Windows.Forms.ToolStripButton();
        	this.複製CToolStripButton = new System.Windows.Forms.ToolStripButton();
        	this.貼上PToolStripButton = new System.Windows.Forms.ToolStripButton();
        	this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
        	this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
        	this.toolStripDropDownButton2 = new System.Windows.Forms.ToolStripDropDownButton();
        	this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
        	this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
        	this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
        	this.listBox1 = new System.Windows.Forms.ListBox();
        	this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
        	this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
        	this.toolStrip1.SuspendLayout();
        	this.SuspendLayout();
        	// 
        	// toolStrip1
        	// 
        	this.toolStrip1.BackColor = System.Drawing.SystemColors.Control;
        	this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
        	        	        	this.新增NToolStripButton,
        	        	        	this.開啟OToolStripButton,
        	        	        	this.儲存SToolStripButton,
        	        	        	this.toolStripButton1,
        	        	        	this.toolStripSeparator1,
        	        	        	this.StartStripButton,
        	        	        	this.StopStripButton,
        	        	        	this.toolStripSeparator,
        	        	        	this.剪下UToolStripButton,
        	        	        	this.複製CToolStripButton,
        	        	        	this.貼上PToolStripButton,
        	        	        	this.toolStripSeparator2,
        	        	        	this.toolStripDropDownButton1,
        	        	        	this.toolStripDropDownButton2,
        	        	        	this.toolStripSeparator3,
        	        	        	this.toolStripButton4,
        	        	        	this.toolStripButton5});
        	this.toolStrip1.Location = new System.Drawing.Point(0, 0);
        	this.toolStrip1.Name = "toolStrip1";
        	this.toolStrip1.Size = new System.Drawing.Size(384, 25);
        	this.toolStrip1.TabIndex = 0;
        	this.toolStrip1.Text = "toolStrip1";
        	// 
        	// 新增NToolStripButton
        	// 
        	this.新增NToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        	this.新增NToolStripButton.Image = global::ClipboardDetector.Properties.Resources.NewFile_6276;
        	this.新增NToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.新增NToolStripButton.Name = "新增NToolStripButton";
        	this.新增NToolStripButton.Size = new System.Drawing.Size(23, 22);
        	this.新增NToolStripButton.Text = "新增(&N)";
        	this.新增NToolStripButton.Click += new System.EventHandler(this.新增NToolStripButton_Click);
        	// 
        	// 開啟OToolStripButton
        	// 
        	this.開啟OToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        	this.開啟OToolStripButton.Image = global::ClipboardDetector.Properties.Resources.Open_6529;
        	this.開啟OToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
        	this.開啟OToolStripButton.Name = "開啟OToolStripButton";
        	this.開啟OToolStripButton.Size = new System.Drawing.Size(23, 22);
        	this.開啟OToolStripButton.Text = "開啟(&O)";
        	this.開啟OToolStripButton.Click += new System.EventHandler(this.開啟OToolStripButton_Click);
        	// 
        	// 儲存SToolStripButton
        	// 
        	this.儲存SToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        	this.儲存SToolStripButton.Image = global::ClipboardDetector.Properties.Resources.Save_6530;
        	this.儲存SToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
        	this.儲存SToolStripButton.Name = "儲存SToolStripButton";
        	this.儲存SToolStripButton.Size = new System.Drawing.Size(23, 22);
        	this.儲存SToolStripButton.Text = "儲存(&S)";
        	this.儲存SToolStripButton.Click += new System.EventHandler(this.儲存SToolStripButton_Click);
        	// 
        	// toolStripButton1
        	// 
        	this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        	this.toolStripButton1.Image = global::ClipboardDetector.Properties.Resources.SaveFileDialogControl_703;
        	this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Black;
        	this.toolStripButton1.Name = "toolStripButton1";
        	this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
        	this.toolStripButton1.Text = "另存新檔";
        	this.toolStripButton1.Click += new System.EventHandler(this.ToolStripButton1_Click);
        	// 
        	// toolStripSeparator1
        	// 
        	this.toolStripSeparator1.Name = "toolStripSeparator1";
        	this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
        	// 
        	// StartStripButton
        	// 
        	this.StartStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        	this.StartStripButton.Image = global::ClipboardDetector.Properties.Resources.StatusAnnotations_Play_16xLG_color;
        	this.StartStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.StartStripButton.Name = "StartStripButton";
        	this.StartStripButton.Size = new System.Drawing.Size(23, 22);
        	this.StartStripButton.Text = "開始";
        	this.StartStripButton.Click += new System.EventHandler(this.StartStripButton_Click);
        	// 
        	// StopStripButton
        	// 
        	this.StopStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        	this.StopStripButton.Enabled = false;
        	this.StopStripButton.Image = global::ClipboardDetector.Properties.Resources.StatusAnnotations_Stop_16xLG_color;
        	this.StopStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.StopStripButton.Name = "StopStripButton";
        	this.StopStripButton.Size = new System.Drawing.Size(23, 22);
        	this.StopStripButton.Text = "停止";
        	this.StopStripButton.Click += new System.EventHandler(this.StopStripButton_Click);
        	// 
        	// toolStripSeparator
        	// 
        	this.toolStripSeparator.Name = "toolStripSeparator";
        	this.toolStripSeparator.Size = new System.Drawing.Size(6, 25);
        	// 
        	// 剪下UToolStripButton
        	// 
        	this.剪下UToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        	this.剪下UToolStripButton.Enabled = false;
        	this.剪下UToolStripButton.Image = global::ClipboardDetector.Properties.Resources.Cut_6523;
        	this.剪下UToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.剪下UToolStripButton.Name = "剪下UToolStripButton";
        	this.剪下UToolStripButton.Size = new System.Drawing.Size(23, 22);
        	this.剪下UToolStripButton.Text = "剪下(&U)";
        	this.剪下UToolStripButton.Click += new System.EventHandler(this.剪下UToolStripButton_Click);
        	// 
        	// 複製CToolStripButton
        	// 
        	this.複製CToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        	this.複製CToolStripButton.Enabled = false;
        	this.複製CToolStripButton.Image = global::ClipboardDetector.Properties.Resources.Copy_6524;
        	this.複製CToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.複製CToolStripButton.Name = "複製CToolStripButton";
        	this.複製CToolStripButton.Size = new System.Drawing.Size(23, 22);
        	this.複製CToolStripButton.Text = "複製(&C)";
        	this.複製CToolStripButton.Click += new System.EventHandler(this.複製CToolStripButton_Click);
        	// 
        	// 貼上PToolStripButton
        	// 
        	this.貼上PToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        	this.貼上PToolStripButton.Enabled = false;
        	this.貼上PToolStripButton.Image = global::ClipboardDetector.Properties.Resources.Paste_6520;
        	this.貼上PToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.貼上PToolStripButton.Name = "貼上PToolStripButton";
        	this.貼上PToolStripButton.Size = new System.Drawing.Size(23, 22);
        	this.貼上PToolStripButton.Text = "貼上(&P)";
        	this.貼上PToolStripButton.Click += new System.EventHandler(this.貼上PToolStripButton_Click);
        	// 
        	// toolStripSeparator2
        	// 
        	this.toolStripSeparator2.Name = "toolStripSeparator2";
        	this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
        	// 
        	// toolStripDropDownButton1
        	// 
        	this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        	this.toolStripDropDownButton1.Image = global::ClipboardDetector.Properties.Resources.SortAscending_275;
        	this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
        	this.toolStripDropDownButton1.Size = new System.Drawing.Size(29, 22);
        	this.toolStripDropDownButton1.Text = "toolStripDropDownButton1";
        	this.toolStripDropDownButton1.Visible = false;
        	// 
        	// toolStripDropDownButton2
        	// 
        	this.toolStripDropDownButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        	this.toolStripDropDownButton2.Image = global::ClipboardDetector.Properties.Resources.SortDescending_276;
        	this.toolStripDropDownButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.toolStripDropDownButton2.Name = "toolStripDropDownButton2";
        	this.toolStripDropDownButton2.Size = new System.Drawing.Size(29, 22);
        	this.toolStripDropDownButton2.Text = "toolStripDropDownButton2";
        	this.toolStripDropDownButton2.Visible = false;
        	// 
        	// toolStripSeparator3
        	// 
        	this.toolStripSeparator3.Name = "toolStripSeparator3";
        	this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
        	this.toolStripSeparator3.Visible = false;
        	// 
        	// toolStripButton4
        	// 
        	this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        	this.toolStripButton4.Enabled = false;
        	this.toolStripButton4.Image = global::ClipboardDetector.Properties.Resources.Clearallrequests_8816;
        	this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.toolStripButton4.Name = "toolStripButton4";
        	this.toolStripButton4.Size = new System.Drawing.Size(23, 22);
        	this.toolStripButton4.Text = "刪除";
        	this.toolStripButton4.Click += new System.EventHandler(this.ToolStripButton4_Click);
        	// 
        	// toolStripButton5
        	// 
        	this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        	this.toolStripButton5.Image = global::ClipboardDetector.Properties.Resources.Clearwindowcontent_6304;
        	this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.toolStripButton5.Name = "toolStripButton5";
        	this.toolStripButton5.Size = new System.Drawing.Size(23, 22);
        	this.toolStripButton5.Text = "清除全部";
        	this.toolStripButton5.Click += new System.EventHandler(this.ToolStripButton5_Click);
        	// 
        	// listBox1
        	// 
        	this.listBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
        	        	        	| System.Windows.Forms.AnchorStyles.Left) 
        	        	        	| System.Windows.Forms.AnchorStyles.Right)));
        	this.listBox1.FormattingEnabled = true;
        	this.listBox1.ItemHeight = 12;
        	this.listBox1.Location = new System.Drawing.Point(12, 28);
        	this.listBox1.Name = "listBox1";
        	this.listBox1.Size = new System.Drawing.Size(360, 220);
        	this.listBox1.TabIndex = 1;
        	this.listBox1.SelectedIndexChanged += new System.EventHandler(this.ListBox1_SelectedIndexChanged);
        	// 
        	// openFileDialog1
        	// 
        	this.openFileDialog1.DefaultExt = "txt";
        	this.openFileDialog1.Filter = "純文字檔案|*.txt";
        	// 
        	// saveFileDialog1
        	// 
        	this.saveFileDialog1.DefaultExt = "txt";
        	this.saveFileDialog1.Filter = "純文字檔案|*.txt";
        	// 
        	// FormMain
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.ClientSize = new System.Drawing.Size(384, 262);
        	this.Controls.Add(this.listBox1);
        	this.Controls.Add(this.toolStrip1);
        	this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
        	this.Name = "FormMain";
        	this.Text = "ClipboardDetector";
        	this.Activated += new System.EventHandler(this.FormMain_Activated);
        	this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
        	this.toolStrip1.ResumeLayout(false);
        	this.toolStrip1.PerformLayout();
        	this.ResumeLayout(false);
        	this.PerformLayout();
        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton 新增NToolStripButton;
        private System.Windows.Forms.ToolStripButton 開啟OToolStripButton;
        private System.Windows.Forms.ToolStripButton 儲存SToolStripButton;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton StartStripButton;
        private System.Windows.Forms.ToolStripButton StopStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripButton 剪下UToolStripButton;
        private System.Windows.Forms.ToolStripButton 複製CToolStripButton;
        private System.Windows.Forms.ToolStripButton 貼上PToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}

